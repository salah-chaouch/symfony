<?php

namespace Cremor\CremorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BonLivraison
 *
 * @ORM\Table(name="bon_livraison")
 * @ORM\Entity(repositoryClass="Cremor\CremorBundle\Repository\BonLivraisonRepository")
 */
class BonLivraison
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="qte", type="integer")
     */
    private $qte;

    /**
     * @var string
     *
     * @ORM\Column(name="designation", type="string", length=255)
     */
    private $designation;

    /**
     * @var float
     *
     * @ORM\Column(name="pu", type="float")
     */
    private $pu;

    /**
     * @ORM\ManyToOne(targetEntity="Cremor\CremorBundle\Entity\AppelOffre")
     * @ORM\JoinColumn(nullable=false)
     */
    private $appelOffre;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set qte
     *
     * @param integer $qte
     *
     * @return BonLivraison
     */
    public function setQte($qte)
    {
        $this->qte = $qte;

        return $this;
    }

    /**
     * Get qte
     *
     * @return int
     */
    public function getQte()
    {
        return $this->qte;
    }

    /**
     * Set designation
     *
     * @param string $designation
     *
     * @return BonLivraison
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * Get designation
     *
     * @return string
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Set pu
     *
     * @param float $pu
     *
     * @return BonLivraison
     */
    public function setPu($pu)
    {
        $this->pu = $pu;

        return $this;
    }

    /**
     * Get pu
     *
     * @return float
     */
    public function getPu()
    {
        return $this->pu;
    }

    /**
     * Set appelOffre
     *
     * @param \Cremor\CremorBundle\Entity\AppelOffre $appelOffre
     *
     * @return BonLivraison
     */
    public function setAppelOffre(\Cremor\CremorBundle\Entity\AppelOffre $appelOffre)
    {
        $this->appelOffre = $appelOffre;

        return $this;
    }

    /**
     * Get appelOffre
     *
     * @return \Cremor\CremorBundle\Entity\AppelOffre
     */
    public function getAppelOffre()
    {
        return $this->appelOffre;
    }
}
