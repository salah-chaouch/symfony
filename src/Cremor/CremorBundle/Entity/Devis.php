<?php

namespace Cremor\CremorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Devis
 *
 * @ORM\Table(name="devis")
 * @ORM\Entity(repositoryClass="Cremor\CremorBundle\Repository\DevisRepository")
 */
class Devis
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="numDevis", type="string", length=255)
     */
    private $numDevis;

    /**
     * @var string
     *
     * @ORM\Column(name="date", type="string", length=255)
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity="Cremor\CremorBundle\Entity\BordereauxPrix")
     * @ORM\JoinColumn(nullable=false)
     */
    private $bordereauxPrix;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numDevis
     *
     * @param string $numDevis
     *
     * @return Devis
     */
    public function setNumDevis($numDevis)
    {
        $this->numDevis = $numDevis;

        return $this;
    }

    /**
     * Get numDevis
     *
     * @return string
     */
    public function getNumDevis()
    {
        return $this->numDevis;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Devis
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set bordereauxPrix
     *
     * @param \Cremor\CremorBundle\Entity\BordereauxPrix $bordereauxPrix
     *
     * @return Devis
     */
    public function setBordereauxPrix(\Cremor\CremorBundle\Entity\BordereauxPrix $bordereauxPrix)
    {
        $this->bordereauxPrix = $bordereauxPrix;

        return $this;
    }

    /**
     * Get bordereauxPrix
     *
     * @return \Cremor\CremorBundle\Entity\BordereauxPrix
     */
    public function getBordereauxPrix()
    {
        return $this->bordereauxPrix;
    }
}
