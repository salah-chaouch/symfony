<?php

namespace Cremor\CremorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Poste
 *
 * @ORM\Table(name="Poste")
 * @ORM\Entity(repositoryClass="Cremor\CremorBundle\Repository\PosteRepository")
 */
class Poste
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="designation", type="string", length=255)
     */
    private $designation;

    /**
     * @var int
     *
     * @ORM\Column(name="qt", type="integer")
     */
    private $qt;

    /**
     * @var float
     *
     * @ORM\Column(name="pu", type="float")
     */
    private $pu;

    /**
     * @ORM\ManyToOne(targetEntity="Cremor\CremorBundle\Entity\BordereauxPrix", cascade={"persist"})
     */
    private $bordereauxPrix;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set designation
     *
     * @param string $designation
     *
     * @return Poste
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * Get designation
     *
     * @return string
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Set qt
     *
     * @param integer $qt
     *
     * @return Poste
     */
    public function setQt($qt)
    {
        $this->qt = $qt;

        return $this;
    }

    /**
     * Get qt
     *
     * @return int
     */
    public function getQt()
    {
        return $this->qt;
    }

    /**
     * Set pu
     *
     * @param float $pu
     *
     * @return Poste
     */
    public function setPu($pu)
    {
        $this->pu = $pu;

        return $this;
    }

    /**
     * Get pu
     *
     * @return float
     */
    public function getPu()
    {
        return $this->pu;
    }

    /**
     * Set bordereauxPrix
     *
     * @param \Cremor\CremorBundle\Entity\BordereauxPrix $bordereauxPrix
     *
     * @return Poste
     */
    public function setBordereauxPrix(\Cremor\CremorBundle\Entity\BordereauxPrix $bordereauxPrix = null)
    {
        $this->bordereauxPrix = $bordereauxPrix;

        return $this;
    }

    /**
     * Get bordereauxPrix
     *
     * @return \Cremor\CremorBundle\Entity\BordereauxPrix
     */
    public function getBordereauxPrix()
    {
        return $this->bordereauxPrix;
    }
}
