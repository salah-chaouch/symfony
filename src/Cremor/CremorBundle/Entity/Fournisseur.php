<?php

namespace Cremor\CremorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Fournisseur
 *
 * @ORM\Table(name="fournisseur")
 * @ORM\Entity(repositoryClass="Cremor\CremorBundle\Repository\FournisseurRepository")
 */
class Fournisseur
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nomSociete", type="string", length=255)
     */
    private $nomSociete;

    /**
     * @var string
     *
     * @ORM\Column(name="tel", type="string", length=255)
     */
    private $tel;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=255)
     */
    private $adresse;

    /**
     * @var string
     *
     * @ORM\Column(name="fax", type="string", length=255)
     */
    private $fax;

    /**
     * @ORM\ManyToOne(targetEntity="Cremor\CremorBundle\Entity\DemmandePrix")
     * @ORM\JoinColumn(nullable=false)
     */
    private $demmandePrix;

    /**
     * @ORM\ManyToOne(targetEntity="Cremor\CremorBundle\Entity\AppelOffre")
     * @ORM\JoinColumn(nullable=false)
     */
    private $appelOffre;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nomSociete
     *
     * @param string $nomSociete
     *
     * @return Fournisseur
     */
    public function setNomSociete($nomSociete)
    {
        $this->nomSociete = $nomSociete;

        return $this;
    }

    /**
     * Get nomSociete
     *
     * @return string
     */
    public function getNomSociete()
    {
        return $this->nomSociete;
    }

    /**
     * Set tel
     *
     * @param string $tel
     *
     * @return Fournisseur
     */
    public function setTel($tel)
    {
        $this->tel = $tel;

        return $this;
    }

    /**
     * Get tel
     *
     * @return string
     */
    public function getTel()
    {
        return $this->tel;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     *
     * @return Fournisseur
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set fax
     *
     * @param string $fax
     *
     * @return Fournisseur
     */
    public function setFax($fax)
    {
        $this->fax = $fax;

        return $this;
    }

    /**
     * Get fax
     *
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set demmandePrix
     *
     * @param \Cremor\CremorBundle\Entity\DemmandePrix $demmandePrix
     *
     * @return Fournisseur
     */
    public function setDemmandePrix(\Cremor\CremorBundle\Entity\DemmandePrix $demmandePrix)
    {
        $this->demmandePrix = $demmandePrix;

        return $this;
    }

    /**
     * Get demmandePrix
     *
     * @return \Cremor\CremorBundle\Entity\DemmandePrix
     */
    public function getDemmandePrix()
    {
        return $this->demmandePrix;
    }

    /**
     * Set appelOffre
     *
     * @param \Cremor\CremorBundle\Entity\AppelOffre $appelOffre
     *
     * @return Fournisseur
     */
    public function setAppelOffre(\Cremor\CremorBundle\Entity\AppelOffre $appelOffre)
    {
        $this->appelOffre = $appelOffre;

        return $this;
    }

    /**
     * Get appelOffre
     *
     * @return \Cremor\CremorBundle\Entity\AppelOffre
     */
    public function getAppelOffre()
    {
        return $this->appelOffre;
    }
}
