<?php

namespace Cremor\CremorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BordereauxPrix
 *
 * @ORM\Table(name="bordereaux_prix")
 * @ORM\Entity(repositoryClass="Cremor\CremorBundle\Repository\BordereauxPrixRepository")
 */
class BordereauxPrix
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="date", type="string", length=255)
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity="Cremor\CremorBundle\Entity\AppelOffre")
     * @ORM\JoinColumn(nullable=false)
     */
    private $appelOffre;




    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return BordereauxPrix
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->poste = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set appelOffre
     *
     * @param \Cremor\CremorBundle\Entity\AppelOffre $appelOffre
     *
     * @return BordereauxPrix
     */
    public function setAppelOffre(\Cremor\CremorBundle\Entity\AppelOffre $appelOffre)
    {
        $this->appelOffre = $appelOffre;

        return $this;
    }

    /**
     * Get appelOffre
     *
     * @return \Cremor\CremorBundle\Entity\AppelOffre
     */
    public function getAppelOffre()
    {
        return $this->appelOffre;
    }
}
