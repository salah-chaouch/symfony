<?php

namespace Cremor\CremorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DemmandeDevis
 *
 * @ORM\Table(name="demmande_devis")
 * @ORM\Entity(repositoryClass="Cremor\CremorBundle\Repository\DemmandeDevisRepository")
 */
class DemmandeDevis
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="date", type="string", length=255)
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity="Cremor\CremorBundle\Entity\Fournisseur")
     * @ORM\JoinColumn(nullable=false)
     */
    private $fournisseur;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return DemmandeDevis
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set fournisseur
     *
     * @param \Cremor\CremorBundle\Entity\Fournisseur $fournisseur
     *
     * @return DemmandeDevis
     */
    public function setFournisseur(\Cremor\CremorBundle\Entity\Fournisseur $fournisseur)
    {
        $this->fournisseur = $fournisseur;

        return $this;
    }

    /**
     * Get fournisseur
     *
     * @return \Cremor\CremorBundle\Entity\Fournisseur
     */
    public function getFournisseur()
    {
        return $this->fournisseur;
    }
}
