<?php

namespace Cremor\CremorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * DevisFournisseur
 *
 * @ORM\Table(name="devis_fournisseur")
 * @ORM\Entity(repositoryClass="Cremor\CremorBundle\Repository\DevisFournisseurRepository")
 */
class DevisFournisseur
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="pieceJointe", type="string", length=255)
     */
    private $pieceJointe;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity="Cremor\CremorBundle\Entity\DemmandePrix")
     * @ORM\JoinColumn(nullable=false)
     */
    private $demmandePrix;

    private $file;

    public function getFile()
    {
        return $this->file;
    }

    public function upload()
    {
        // Si jamais il n'y a pas de fichier (champ facultatif), on ne fait rien
        if (null === $this->file) {
            return;
        }

        // On récupère le nom original du fichier de l'internaute
        $pieceJointe = $this->file->getClientOriginalName();

        // On déplace le fichier envoyé dans le répertoire de notre choix
        $this->file->move($this->getUploadRootDir(), $pieceJointe);

    }

    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    public function getUploadDir()
    {
        return 'uploads/img/piecesJoints';
    }

    protected function getUploadRootDir()
    {
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pieceJointe
     *
     * @param string $pieceJointe
     *
     * @return DevisFournisseur
     */
    public function setPieceJointe($pieceJointe)
    {
        $this->pieceJointe = $pieceJointe;

        return $this;
    }

    /**
     * Get pieceJointe
     *
     * @return string
     */
    public function getPieceJointe()
    {
        return $this->pieceJointe;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return DevisFournisseur
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set demmandePrix
     *
     * @param \Cremor\CremorBundle\Entity\DemmandePrix $demmandePrix
     *
     * @return DevisFournisseur
     */
    public function setDemmandePrix(\Cremor\CremorBundle\Entity\DemmandePrix $demmandePrix)
    {
        $this->demmandePrix = $demmandePrix;

        return $this;
    }

    /**
     * Get demmandePrix
     *
     * @return \Cremor\CremorBundle\Entity\DemmandePrix
     */
    public function getDemmandePrix()
    {
        return $this->demmandePrix;
    }
}
