<?php

namespace Cremor\CremorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FactureClient
 *
 * @ORM\Table(name="facture_client")
 * @ORM\Entity(repositoryClass="Cremor\CremorBundle\Repository\FactureClientRepository")
 */
class FactureClient
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="qte", type="integer")
     */
    private $qte;

    /**
     * @var string
     *
     * @ORM\Column(name="designation", type="string", length=255)
     */
    private $designation;

    /**
     * @ORM\ManyToOne(targetEntity="Cremor\CremorBundle\Entity\AppelOffre")
     * @ORM\JoinColumn(nullable=false)
     */
    private $appelOffre;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set qte
     *
     * @param integer $qte
     *
     * @return FactureClient
     */
    public function setQte($qte)
    {
        $this->qte = $qte;

        return $this;
    }

    /**
     * Get qte
     *
     * @return int
     */
    public function getQte()
    {
        return $this->qte;
    }

    /**
     * Set designation
     *
     * @param string $designation
     *
     * @return FactureClient
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * Get designation
     *
     * @return string
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Set appelOffre
     *
     * @param \Cremor\CremorBundle\Entity\AppelOffre $appelOffre
     *
     * @return FactureClient
     */
    public function setAppelOffre(\Cremor\CremorBundle\Entity\AppelOffre $appelOffre)
    {
        $this->appelOffre = $appelOffre;

        return $this;
    }

    /**
     * Get appelOffre
     *
     * @return \Cremor\CremorBundle\Entity\AppelOffre
     */
    public function getAppelOffre()
    {
        return $this->appelOffre;
    }
}
