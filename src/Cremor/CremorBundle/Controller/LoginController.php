<?php

namespace Cremor\CremorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Cremor\CremorBundle\Entity\User;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class LoginController extends Controller
{
    public function indexAction(Request $request)
    {
        $session = new Session();
        if($session->has("role")){
            $session->get("role");
            return $this->render('CremorBundle:Index:index.html.twig');
        }else{
            $user = new User();
        $formBuilder = $this->get('form.factory')->createBuilder(FormType::class, $user);
        $formBuilder
            ->add('login',TextType::class)
            ->add('password',TextType::class)
            ->add('SignIn',SubmitType::class);
        $form = $formBuilder->getForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $user = $em->getRepository('CremorBundle:User')->Exists($user->getLogin(),$user->getPassword());
                if($user!=NULL){
                    $session = new Session();
                    $role = $user->getRole();
                    $session->set("role",$role);
                    return $this->redirectToRoute('cremor_listing_client');
                }
            }
        }
        return $this->render('CremorBundle:Login:login.html.twig', array(
            'form' => $form->createView(),
        ));
        }
    }
    public function deconnectAction(){
        $session = new Session();
        $session->invalidate();
        return $this->redirectToRoute('cremor_login');
    }
}
