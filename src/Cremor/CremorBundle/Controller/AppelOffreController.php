<?php

namespace Cremor\CremorBundle\Controller;

use Cremor\CremorBundle\Entity\AppelOffre;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class AppelOffreController extends Controller
{
    public function addAction(Request $request)
    {
        $appelOffre = new AppelOffre();
        $formBuilder = $this->get('form.factory')->createBuilder(FormType::class, $appelOffre);
        $formBuilder
            ->add('numAO',TextType::class)
            ->add('objet',TextType::class)
            ->add('date',TextType::class)
            ->add('client',EntityType::class, array(
                'class'         => 'CremorBundle:Client',
                'choice_label'  => 'nomSociete',
            ))
            ->add('save',SubmitType::class)
        ;
        $form = $formBuilder->getForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($appelOffre);
                $em->flush();
                return $this->redirectToRoute('cremor_listing_AppelOffre');
            }
        }
        return $this->render('CremorBundle:AppelOffre:add.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $appelOffre = $em->getRepository('CremorBundle:AppelOffre')->find($id);
        $formBuilder = $this->get('form.factory')->createBuilder(FormType::class, $appelOffre);
        $formBuilder
            ->add('numAO',TextType::class)
            ->add('objet',TextType::class)
            ->add('date',TextType::class)
            ->add('client',EntityType::class, array(
                'class'         => 'CremorBundle:Client',
                'choice_label'  => 'nomSociete',
            ))
            ->add('save',SubmitType::class)
        ;
        $form = $formBuilder->getForm();

        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $em->flush();
            return $this->redirectToRoute('cremor_listing_AppelOffre');
        }

        return $this->render('CremorBundle:AppelOffre:edit.html.twig', array(
            'appelOffre' => $appelOffre,
            'form'   => $form->createView(),
        ));
    }

    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $appelOffre = $em->getRepository('CremorBundle:AppelOffre')->find($id);
        $em->remove($appelOffre);
        $em->flush();

        return $this->render('CremorBundle:AppelOffre:ao.html.twig', array(
            'appelOffre' => $appelOffre,
        ));
    }

    public function listingAction()
    {
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);
        $repository = $this
            ->getDoctrine()
            ->getRepository('CremorBundle:AppelOffre');

        $AOs = $repository->findAll();
        $jsonContent = $serializer->serialize($AOs, 'json');
        return $this->render('CremorBundle:AppelOffre:ao.html.twig', array(
            'data' => $jsonContent
        ));
    }
}
