<?php

namespace Cremor\CremorBundle\Controller;

use Cremor\CremorBundle\Entity\Client;
use Cremor\CremorBundle\Entity\Poste;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;


class PosteController extends Controller
{
    public function addAction(Request $request)
    {
        $poste = new Poste();
        $formBuilder = $this->get('form.factory')->createBuilder(FormType::class, $poste);
        $formBuilder
            ->add('designation',TextType::class)
            ->add('qt',TextType::class)
            ->add('pu',TextType::class)
            ->add('bordereauxPrix',EntityType::class, array(
                'class'         => 'CremorBundle:BordereauxPrix',
                'choice_label'  => 'id',
            ))
            ->add('save',SubmitType::class)
        ;
        $form = $formBuilder->getForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($poste);
                $em->flush();
                //$request->getSession()->getFlashBag()->add('notice', 'Annonce bien enregistrée.');
                //return $this->redirectToRoute('oc_platform_view', array('id' => $advert->getId()));
            }
        }
        return $this->render('CremorBundle:Poste:add.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $poste = $em->getRepository('CremorBundle:Poste')->find($id);

        /*if (null === $client) {
            throw new NotFoundHttpException("L'annonce d'id ".$id." n'existe pas.");
        }*/

        $formBuilder = $this->get('form.factory')->createBuilder(FormType::class, $poste);
        $formBuilder
            ->add('designation',TextType::class)
            ->add('qt',TextType::class)
            ->add('pu',TextType::class)
            ->add('bordereauxPrix',EntityType::class, array(
                'class'         => 'CremorBundle:BordereauxPrix',
                'choice_label'  => 'designation',
            ))
            ->add('save',SubmitType::class)
        ;
        $form = $formBuilder->getForm();

        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $em->flush();
            //$request->getSession()->getFlashBag()->add('notice', 'Annonce bien modifiée.');
            //return $this->redirectToRoute('oc_platform_view', array('id' => $advert->getId()));
        }

        return $this->render('CremorBundle:Poste:edit.html.twig', array(
            'poste' => $poste,
            'form'   => $form->createView(),
        ));
    }

    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $poste = $em->getRepository('CremorBundle:Poste')->find($id);

        /*if (null === $advert) {
            throw new NotFoundHttpException("L'annonce d'id ".$id." n'existe pas.");
        }*/

        // On crée un formulaire vide, qui ne contiendra que le champ CSRF
        // Cela permet de protéger la suppression d'annonce contre cette faille


            $em->remove($poste);
            $em->flush();
            /*
            $request->getSession()->getFlashBag()->add('info', "L'annonce a bien été supprimée.");
            return $this->redirectToRoute('oc_platform_home');*/


        return $this->render('CremorBundle:Poste:index.html.twig', array(
            'poste' => $poste,
        ));
    }



    public function listingAction()
    {
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);
        $repository = $this
            ->getDoctrine()
            ->getRepository('CremorBundle:Poste');

        $AOs = $repository->findAll();
        $jsonContent = $serializer->serialize($AOs, 'json');
        return $this->render('CremorBundle:Poste:poste.html.twig', array(
            'data' => $jsonContent
        ));
    }

}
