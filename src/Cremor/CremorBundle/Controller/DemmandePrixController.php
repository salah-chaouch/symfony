<?php

namespace Cremor\CremorBundle\Controller;

use Cremor\CremorBundle\Entity\DemmandePrix;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class DemmandePrixController extends Controller
{
    public function addAction(Request $request)
    {
        $demmandePrix = new DemmandePrix();
        $formBuilder = $this->get('form.factory')->createBuilder(FormType::class, $demmandePrix);
        $formBuilder
            ->add('date',TextType::class)
            ->add('destinataire',TextType::class)
            ->add('societe',TextType::class)
            ->add('save',SubmitType::class)
        ;
        $form = $formBuilder->getForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($demmandePrix);
                $em->flush();
            }
        }
        return $this->render('CremorBundle:DemmandePrix:add.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $demmandePrix = $em->getRepository('CremorBundle:DemmandePrix')->find($id);

        $formBuilder = $this->get('form.factory')->createBuilder(FormType::class, $demmandePrix);
        $formBuilder
            ->add('date',TextType::class)
            ->add('destinataire',TextType::class)
            ->add('societe',TextType::class)
            ->add('save',SubmitType::class)
        ;
        $form = $formBuilder->getForm();

        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $em->flush();
        }

        return $this->render('CremorBundle:DemmandePrix:edit.html.twig', array(
            'client' => $demmandePrix,
            'form'   => $form->createView(),
        ));
    }

    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $client = $em->getRepository('CremorBundle:Client')->find($id);
            $em->remove($client);
            $em->flush();
        return $this->render('CremorBundle:Index:index.html.twig', array(
            'client' => $client,
        ));
    }

    public function listingAction()
    {
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);
        $repository = $this
            ->getDoctrine()
            ->getRepository('CremorBundle:DemmandePrix');

        $AOs = $repository->findAll();
        $jsonContent = $serializer->serialize($AOs, 'json');
        return $this->render('CremorBundle:DemmandePrix:demandePrix.html.twig', array(
            'data' => $jsonContent
        ));
    }
}
