<?php

namespace Cremor\CremorBundle\Controller;

use Cremor\CremorBundle\Entity\AppelOffre;
use Cremor\CremorBundle\Entity\BonLivraison;
use Cremor\CremorBundle\Entity\FactureClient;
use Doctrine\DBAL\Types\DecimalType;
use Doctrine\DBAL\Types\FloatType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class FactureClientController extends Controller
{
    public function addAction(Request $request)
    {
        $factureClient = new FactureClient();
        $formBuilder = $this->get('form.factory')->createBuilder(FormType::class, $factureClient);
        $formBuilder
            ->add('qte',NumberType::class)
            ->add('designation',TextType::class)
            ->add('appelOffre',EntityType::class, array(
                'class'         => 'CremorBundle:AppelOffre',
                'choice_label'  => 'numAO',
            ))
            ->add('save',SubmitType::class)
        ;
        $form = $formBuilder->getForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($factureClient);
                $em->flush();
            }
        }
        return $this->render('CremorBundle:FactureClient:add.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $factureClient = $em->getRepository('CremorBundle:FactureClient')->find($id);
        $formBuilder = $this->get('form.factory')->createBuilder(FormType::class, $factureClient);
        $formBuilder
            ->add('qte',NumberType::class)
            ->add('designation',TextType::class)
            ->add('pu',NumberType::class)
            ->add('appelOffre',EntityType::class, array(
                'class'         => 'CremorBundle:AppelOffre',
                'choice_label'  => 'numAO',
            ))
            ->add('save',SubmitType::class)
        ;
        $form = $formBuilder->getForm();

        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $em->flush();
        }

        return $this->render('CremorBundle:FactureClient:edit.html.twig', array(
            'factureClient' => $factureClient,
            'form'   => $form->createView(),
        ));
    }

    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $factureClient = $em->getRepository('CremorBundle:FactureClient')->find($id);
        $em->remove($factureClient);
        $em->flush();

        return $this->render('CremorBundle:Index:index.html.twig', array(
            'bonLivraison' => $bonLivraison,
        ));
    }

    public function listingAction()
    {
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);
        $repository = $this
            ->getDoctrine()
            ->getRepository('CremorBundle:FactureClient');

        $AOs = $repository->findAll();
        $jsonContent = $serializer->serialize($AOs, 'json');
        return $this->render('CremorBundle:FactureClient:factureClient.html.twig', array(
            'data' => $jsonContent
        ));
    }
}
