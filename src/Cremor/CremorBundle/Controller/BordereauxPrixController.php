<?php

namespace Cremor\CremorBundle\Controller;

use Cremor\CremorBundle\Entity\BordereauxPrix;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class BordereauxPrixController extends Controller
{
    public function addAction(Request $request)
    {
        $bordereauxPrix = new BordereauxPrix();
        $formBuilder = $this->get('form.factory')->createBuilder(FormType::class, $bordereauxPrix);
        $formBuilder
            ->add('date',TextType::class)
            ->add('appelOffre',EntityType::class, array(
                'class'         => 'CremorBundle:AppelOffre',
                'choice_label'  => 'numAO',
            ))
            ->add('save',SubmitType::class)
        ;
        $form = $formBuilder->getForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($bordereauxPrix);
                $em->flush();
            }
        }
        return $this->render('CremorBundle:BordereauxPrix:add.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $bordereauxPrix = $em->getRepository('CremorBundle:BordereauxPrix')->find($id);
        $formBuilder = $this->get('form.factory')->createBuilder(FormType::class, $bordereauxPrix);
        $formBuilder
            ->add('date',TextType::class)
            ->add('appelOffre',EntityType::class, array(
                'class'         => 'CremorBundle:AppelOffre',
                'choice_label'  => 'numAO',
            ))
            ->add('poste',EntityType::class, array(
                'class'         => 'CremorBundle:Poste',
                'choice_label'  => 'designation',
            ))
            ->add('save',SubmitType::class)
        ;
        $form = $formBuilder->getForm();

        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $em->flush();
        }

        return $this->render('CremorBundle:BordereauxPrix:edit.html.twig', array(
            'bordereauxPrix' => $bordereauxPrix,
            'form'   => $form->createView(),
        ));
    }

    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $bordereauxPrix = $em->getRepository('CremorBundle:BordereauxPrix')->find($id);
        $em->remove($bordereauxPrix);
        $em->flush();

        return $this->render('CremorBundle:Index:index.html.twig', array(
            'appelOffre' => $bordereauxPrix,
        ));
    }

    public function listingAction()
    {
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);
        $repository = $this
            ->getDoctrine()
            ->getRepository('CremorBundle:BordereauxPrix');

        $AOs = $repository->findAll();
        $jsonContent = $serializer->serialize($AOs, 'json');
        return $this->render('CremorBundle:BordereauxPrix:bordereauxPrix.html.twig', array(
            'data' => $jsonContent
        ));
    }
}
