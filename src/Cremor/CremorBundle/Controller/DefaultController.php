<?php

namespace Cremor\CremorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('CremorBundle:Default:index.html.twig');
    }
}
