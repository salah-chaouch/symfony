<?php

namespace Cremor\CremorBundle\Controller;

use Cremor\CremorBundle\Entity\Commande;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class CommandeController extends Controller
{
    public function addAction(Request $request)
    {
        $commande = new Commande();
        $formBuilder = $this->get('form.factory')->createBuilder(FormType::class, $commande);
        $formBuilder
            ->add('numCommande',TextType::class)
            ->add('date',TextType::class)
            ->add('montant',NumberType::class)
            ->add('appelOffre',EntityType::class, array(
                'class'         => 'CremorBundle:AppelOffre',
                'choice_label'  => 'numAO',
            ))
            ->add('save',SubmitType::class)
        ;
        $form = $formBuilder->getForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($commande);
                $em->flush();
            }
        }
        return $this->render('CremorBundle:Commande:add.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $commande = $em->getRepository('CremorBundle:Commande')->find($id);
        $formBuilder = $this->get('form.factory')->createBuilder(FormType::class, $commande);
        $formBuilder
            ->add('numCommande',TextType::class)
            ->add('date',TextType::class)
            ->add('montant',NumberType::class)
            ->add('appelOffre',EntityType::class, array(
                'class'         => 'CremorBundle:AppelOffre',
                'choice_label'  => 'numAO',
            ))
            ->add('save',SubmitType::class)
        ;
        $form = $formBuilder->getForm();

        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $em->flush();
        }

        return $this->render('CremorBundle:Commande:edit.html.twig', array(
            'commande' => $commande,
            'form'   => $form->createView(),
        ));
    }

    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $commande = $em->getRepository('CremorBundle:Commande')->find($id);
        $em->remove($commande);
        $em->flush();

        return $this->render('CremorBundle:Index:index.html.twig', array(
            'commande' => $commande,
        ));
    }

    public function listingAction()
    {
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);
        $repository = $this
            ->getDoctrine()
            ->getRepository('CremorBundle:Commande');

        $AOs = $repository->findAll();
        $jsonContent = $serializer->serialize($AOs, 'json');
        return $this->render('CremorBundle:Commande:commande.html.twig', array(
            'data' => $jsonContent
        ));
    }
}
