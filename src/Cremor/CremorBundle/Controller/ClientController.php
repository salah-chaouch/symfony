<?php

namespace Cremor\CremorBundle\Controller;

use Cremor\CremorBundle\Entity\Client;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class ClientController extends Controller
{
    public function addAction(Request $request)
    {
        $client = new Client();
        $formBuilder = $this->get('form.factory')->createBuilder(FormType::class, $client);
        $formBuilder
            ->add('nomSociete',TextType::class)
            ->add('tel',TextType::class)
            ->add('adress',TextType::class)
            ->add('fax',TextType::class)
            ->add('save',SubmitType::class)
        ;
        $form = $formBuilder->getForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($client);
                $em->flush();
                return $this->redirectToRoute('cremor_listing_client');
                //$request->getSession()->getFlashBag()->add('notice', 'Annonce bien enregistrée.');
                //return $this->redirectToRoute('oc_platform_view', array('id' => $advert->getId()));
            }
        }
        return $this->render('CremorBundle:Client:add.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $client = $em->getRepository('CremorBundle:Client')->find($id);

        /*if (null === $client) {
            throw new NotFoundHttpException("L'annonce d'id ".$id." n'existe pas.");
        }*/

        $formBuilder = $this->get('form.factory')->createBuilder(FormType::class, $client);
        $formBuilder
            ->add('nomSociete',TextType::class)
            ->add('tel',TextType::class)
            ->add('adress',TextType::class)
            ->add('fax',TextType::class)
            ->add('save',SubmitType::class)
        ;
        $form = $formBuilder->getForm();

        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $em->flush();
            return $this->redirectToRoute('cremor_listing_client');
            //$request->getSession()->getFlashBag()->add('notice', 'Annonce bien modifiée.');
            //return $this->redirectToRoute('oc_platform_view', array('id' => $advert->getId()));
        }

        return $this->render('CremorBundle:Client:edit.html.twig', array(
            'client' => $client,
            'form'   => $form->createView(),
        ));
    }

    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $client = $em->getRepository('CremorBundle:Client')->find($id);

        /*if (null === $advert) {
            throw new NotFoundHttpException("L'annonce d'id ".$id." n'existe pas.");
        }*/

        // On crée un formulaire vide, qui ne contiendra que le champ CSRF
        // Cela permet de protéger la suppression d'annonce contre cette faille


            $em->remove($client);
            $em->flush();
            return $this->redirectToRoute('cremor_listing_client');
            /*
            $request->getSession()->getFlashBag()->add('info', "L'annonce a bien été supprimée.");
            return $this->redirectToRoute('oc_platform_home');*/


        return $this->render('CremorBundle:Index:index.html.twig', array(
            'client' => $client,
        ));
    }



    public function listingAction()
    {
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);
        $repository = $this
            ->getDoctrine()
            ->getRepository('CremorBundle:Client');

        $AOs = $repository->findAll();
        $jsonContent = $serializer->serialize($AOs, 'json');
        return $this->render('CremorBundle:Client:listing.html.twig', array(
            'data' => $jsonContent
        ));
    }

}
