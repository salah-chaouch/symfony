<?php

namespace Cremor\CremorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;

class IndexController extends Controller
{
    public function indexAction()
    {
        $session = new Session();
        if($session->has("role")){
            return $this->render('CremorBundle:Index:index.html.twig');
        }else{
            return $this->redirectToRoute('cremor_login');
        }
    }
}
