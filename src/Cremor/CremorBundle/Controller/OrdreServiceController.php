<?php

namespace Cremor\CremorBundle\Controller;

use Cremor\CremorBundle\Entity\BordereauxPrix;
use Cremor\CremorBundle\Entity\OrdreService;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class OrdreServiceController extends Controller
{
    public function addAction(Request $request)
    {
        $orderService = new OrdreService();
        $formBuilder = $this->get('form.factory')->createBuilder(FormType::class, $orderService);
        $formBuilder
            ->add('numOS',TextType::class)
            ->add('date',TextType::class)
            ->add('appelOffre',EntityType::class, array(
                'class'         => 'CremorBundle:AppelOffre',
                'choice_label'  => 'numAO',
            ))
            ->add('save',SubmitType::class)
        ;
        $form = $formBuilder->getForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($orderService);
                $em->flush();
            }
        }
        return $this->render('CremorBundle:OrdreService:add.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $orderService = $em->getRepository('CremorBundle:OrdreService')->find($id);
        $formBuilder = $this->get('form.factory')->createBuilder(FormType::class, $orderService);
        $formBuilder
            ->add('numOS',TextType::class)
            ->add('date',TextType::class)
            ->add('appelOffre',EntityType::class, array(
                'class'         => 'CremorBundle:AppelOffre',
                'choice_label'  => 'numAO',
            ))
            ->add('save',SubmitType::class)
        ;
        $form = $formBuilder->getForm();

        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $em->flush();
        }

        return $this->render('CremorBundle:OrdreService:edit.html.twig', array(
            'orderService' => $orderService,
            'form'   => $form->createView(),
        ));
    }

    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $orderService = $em->getRepository('CremorBundle:OrdreService')->find($id);
        $em->remove($orderService);
        $em->flush();

        return $this->render('CremorBundle:Index:index.html.twig', array(
            'appelOffre' => $orderService,
        ));
    }

    public function listingAction()
    {
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);
        $repository = $this
            ->getDoctrine()
            ->getRepository('CremorBundle:OrdreService');

        $AOs = $repository->findAll();
        $jsonContent = $serializer->serialize($AOs, 'json');
        return $this->render('CremorBundle:OrdreService:ordreService.html.twig', array(
            'data' => $jsonContent
        ));
    }
}
