<?php

namespace Cremor\CremorBundle\Controller;

use Cremor\CremorBundle\Entity\Commande;
use Cremor\CremorBundle\Entity\FactureFournisseur;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class FactureFournisseurController extends Controller
{
    public function addAction(Request $request)
    {
        $factureFournisseur = new FactureFournisseur();
        $formBuilder = $this->get('form.factory')->createBuilder(FormType::class, $factureFournisseur);
        $formBuilder
            ->add('date',TextType::class)
            ->add('fournisseur',EntityType::class, array(
                'class'         => 'CremorBundle:Fournisseur',
                'choice_label'  => 'nomSociete',
            ))
            ->add('save',SubmitType::class)
        ;
        $form = $formBuilder->getForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($factureFournisseur);
                $em->flush();
            }
        }
        return $this->render('CremorBundle:FactureFournisseur:add.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $factureFournisseur = $em->getRepository('CremorBundle:FactureFournisseur')->find($id);
        $formBuilder = $this->get('form.factory')->createBuilder(FormType::class, $factureFournisseur);
        $formBuilder
            ->add('date',TextType::class)
            ->add('fournisseur',EntityType::class, array(
                'class'         => 'CremorBundle:Fournisseur',
                'choice_label'  => 'nomSociete',
            ))
            ->add('save',SubmitType::class)
        ;
        $form = $formBuilder->getForm();

        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $em->flush();
        }

        return $this->render('CremorBundle:FactureFournisseur:edit.html.twig', array(
            'factureFournisseur' => $factureFournisseur,
            'form'   => $form->createView(),
        ));
    }

    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $factureFournisseur = $em->getRepository('CremorBundle:FactureFournisseur')->find($id);
        $em->remove($factureFournisseur);
        $em->flush();

        return $this->render('CremorBundle:Index:index.html.twig', array(
            'FactureFournisseur' => $factureFournisseur,
        ));
    }

    public function listingAction()
    {
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);
        $repository = $this
            ->getDoctrine()
            ->getRepository('CremorBundle:FactureFournisseur');

        $AOs = $repository->findAll();
        $jsonContent = $serializer->serialize($AOs, 'json');
        return $this->render('CremorBundle:FactureFournisseur:factureFournisseur.html.twig', array(
            'data' => $jsonContent
        ));
    }
}
