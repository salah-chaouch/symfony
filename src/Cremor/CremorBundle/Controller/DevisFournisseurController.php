<?php

namespace Cremor\CremorBundle\Controller;

use Cremor\CremorBundle\Entity\DemmandeDevis;
use Cremor\CremorBundle\Entity\DevisFournisseur;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class DevisFournisseurController extends Controller
{
    public function addAction(Request $request)
    {
        $devisFournisseur = new DevisFournisseur();
        $formBuilder = $this->get('form.factory')->createBuilder(FormType::class, $devisFournisseur);
        $formBuilder
            ->add('date',TextType::class)
            ->add('file',FileType::class)
            ->add('demmandePrix',EntityType::class, array(
                'class'         => 'CremorBundle:DemmandePrix',
                'choice_label'  => 'societe',
            ))
            ->add('save',SubmitType::class)
        ;
        $form = $formBuilder->getForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($devisFournisseur);
                $em->flush();
            }
        }
        return $this->render('CremorBundle:DevisFournisseur:add.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $demmandeDevis = $em->getRepository('CremorBundle:DemmandeDevis')->find($id);
        $formBuilder = $this->get('form.factory')->createBuilder(FormType::class, $demmandeDevis);
        $formBuilder
            ->add('date',TextType::class)
            ->add('fournisseur',EntityType::class, array(
                'class'         => 'CremorBundle:Fournisseur',
                'choice_label'  => 'nomSociete',
            ))
            ->add('save',SubmitType::class)
        ;
        $form = $formBuilder->getForm();

        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $em->flush();
        }

        return $this->render('CremorBundle:DemmandeDevis:edit.html.twig', array(
            'demmandeDevis' => $demmandeDevis,
            'form'   => $form->createView(),
        ));
    }

    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $demmandeDevis = $em->getRepository('CremorBundle:DemmandeDevis')->find($id);
        $em->remove($demmandeDevis);
        $em->flush();

        return $this->render('CremorBundle:Index:index.html.twig', array(
            'demmandeDevis' => $demmandeDevis,
        ));
    }

    public function listingAction()
    {
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);
        $repository = $this
            ->getDoctrine()
            ->getRepository('CremorBundle:DevisFournisseur');

        $AOs = $repository->findAll();
        $jsonContent = $serializer->serialize($AOs, 'json');
        return $this->render('CremorBundle:DevisFournisseur:devisFournisseur.html.twig', array(
            'data' => $jsonContent
        ));
    }
}
