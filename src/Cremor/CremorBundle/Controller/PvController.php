<?php

namespace Cremor\CremorBundle\Controller;

use Cremor\CremorBundle\Entity\BordereauxPrix;
use Cremor\CremorBundle\Entity\pv;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class PvController extends Controller
{
    public function addAction(Request $request)
    {
        $pv = new pv();
        $formBuilder = $this->get('form.factory')->createBuilder(FormType::class, $pv);
        $formBuilder
            ->add('numPv',TextType::class)
            ->add('date',TextType::class)
            ->add('appelOffre',EntityType::class, array(
                'class'         => 'CremorBundle:AppelOffre',
                'choice_label'  => 'numAO',
            ))
            ->add('save',SubmitType::class)
        ;
        $form = $formBuilder->getForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($pv);
                $em->flush();
            }
        }
        return $this->render('CremorBundle:pv:add.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $pv = $em->getRepository('CremorBundle:pv')->find($id);
        $formBuilder = $this->get('form.factory')->createBuilder(FormType::class, $pv);
        $formBuilder
            ->add('numPv',TextType::class)
            ->add('date',TextType::class)
            ->add('appelOffre',EntityType::class, array(
                'class'         => 'CremorBundle:AppelOffre',
                'choice_label'  => 'numAO',
            ))
            ->add('save',SubmitType::class)
        ;
        $form = $formBuilder->getForm();

        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $em->flush();
        }

        return $this->render('CremorBundle:pv:edit.html.twig', array(
            'pv' => $pv,
            'form'   => $form->createView(),
        ));
    }

    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $pv = $em->getRepository('CremorBundle:pv')->find($id);
        $em->remove($pv);
        $em->flush();

        return $this->render('CremorBundle:Index:index.html.twig', array(
            'pv' => $pv,
        ));
    }

    public function listingAction()
    {
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);
        $repository = $this
            ->getDoctrine()
            ->getRepository('CremorBundle:pv');

        $AOs = $repository->findAll();
        $jsonContent = $serializer->serialize($AOs, 'json');
        return $this->render('CremorBundle:pv:pv.html.twig', array(
            'data' => $jsonContent
        ));
    }
}
