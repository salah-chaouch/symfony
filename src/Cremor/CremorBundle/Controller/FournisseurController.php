<?php

namespace Cremor\CremorBundle\Controller;

use Cremor\CremorBundle\Entity\AppelOffre;
use Cremor\CremorBundle\Entity\Fournisseur;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class FournisseurController extends Controller
{
    public function addAction(Request $request)
    {
        $fournisseur = new Fournisseur();
        $formBuilder = $this->get('form.factory')->createBuilder(FormType::class, $fournisseur);
        $formBuilder
            ->add('nomSociete',TextType::class)
            ->add('tel',TextType::class)
            ->add('adresse',TextType::class)
            ->add('fax',TextType::class)
            ->add('demmandePrix',EntityType::class, array(
                'class'         => 'CremorBundle:DemmandePrix',
                'choice_label'  => 'societe',
            ))
            ->add('save',SubmitType::class)
        ;
        $form = $formBuilder->getForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($fournisseur);
                $em->flush();
                return $this->redirectToRoute('cremor_listing_Fournisseur');
            }
        }
        return $this->render('CremorBundle:Fournisseur:add.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $fournisseur = $em->getRepository('CremorBundle:Fournisseur')->find($id);
        $formBuilder = $this->get('form.factory')->createBuilder(FormType::class, $fournisseur);
        $formBuilder
            ->add('nomSociete',TextType::class)
            ->add('tel',TextType::class)
            ->add('adresse',TextType::class)
            ->add('fax',TextType::class)
            ->add('demmandePrix',EntityType::class, array(
                'class'         => 'CremorBundle:DemmandePrix',
                'choice_label'  => 'societe',
            ))
            ->add('save',SubmitType::class)
        ;
        $form = $formBuilder->getForm();

        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $em->flush();
            return $this->redirectToRoute('cremor_listing_Fournisseur');
        }

        return $this->render('CremorBundle:Fournisseur:edit.html.twig', array(
            'fournisseur' => $fournisseur,
            'form'   => $form->createView(),
        ));
    }

    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $fournisseur = $em->getRepository('CremorBundle:Fournisseur')->find($id);
        $em->remove($fournisseur);
        $em->flush();

        return $this->render('CremorBundle:Index:index.html.twig', array(
            'fournisseur' => $fournisseur,
        ));
    }

    public function listingAction()
    {
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);
        $repository = $this
            ->getDoctrine()
            ->getRepository('CremorBundle:Fournisseur');

        $AOs = $repository->findAll();
        $jsonContent = $serializer->serialize($AOs, 'json');
        return $this->render('CremorBundle:Fournisseur:fournisseur.html.twig', array(
            'data' => $jsonContent
        ));
    }
}
