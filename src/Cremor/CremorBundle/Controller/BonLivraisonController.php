<?php

namespace Cremor\CremorBundle\Controller;

use Cremor\CremorBundle\Entity\AppelOffre;
use Cremor\CremorBundle\Entity\BonLivraison;
use Doctrine\DBAL\Types\DecimalType;
use Doctrine\DBAL\Types\FloatType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class BonLivraisonController extends Controller
{
    public function addAction(Request $request)
    {
        $bonLivraison = new BonLivraison();
        $formBuilder = $this->get('form.factory')->createBuilder(FormType::class, $bonLivraison);
        $formBuilder
            ->add('qte',NumberType::class)
            ->add('designation',TextType::class)
            ->add('pu',NumberType::class)
            ->add('appelOffre',EntityType::class, array(
                'class'         => 'CremorBundle:AppelOffre',
                'choice_label'  => 'numAO',
            ))
            ->add('save',SubmitType::class)
        ;
        $form = $formBuilder->getForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($bonLivraison);
                $em->flush();
                return $this->redirectToRoute('cremor_listing_BonLivraison');
            }
        }
        return $this->render('CremorBundle:BonLivraison:add.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $bonLivraison = $em->getRepository('CremorBundle:BonLivraison')->find($id);
        $formBuilder = $this->get('form.factory')->createBuilder(FormType::class, $bonLivraison);
        $formBuilder
            ->add('qte',NumberType::class)
            ->add('designation',TextType::class)
            ->add('pu',NumberType::class)
            ->add('appelOffre',EntityType::class, array(
                'class'         => 'CremorBundle:AppelOffre',
                'choice_label'  => 'numAO',
            ))
            ->add('save',SubmitType::class)
        ;
        $form = $formBuilder->getForm();

        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $em->flush();
            return $this->redirectToRoute('cremor_listing_BonLivraison');
        }

        return $this->render('CremorBundle:BonLivraison:edit.html.twig', array(
            'bonLivraison' => $bonLivraison,
            'form'   => $form->createView(),
        ));
    }

    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $bonLivraison = $em->getRepository('CremorBundle:BonLivraison')->find($id);
        $em->remove($bonLivraison);
        $em->flush();
        $x = $this->listingAction();
        return $this->render('CremorBundle:BonLivraison:bonLivraison.html.twig', array(
            'bonLivraison' => $bonLivraison,
            'data' => $x
        ));
    }

    public function listingAction()
    {
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);
        $repository = $this
            ->getDoctrine()
            ->getRepository('CremorBundle:BonLivraison');

        $AOs = $repository->findAll();
        $jsonContent = $serializer->serialize($AOs, 'json');
        return $this->render('CremorBundle:BonLivraison:bonLivraison.html.twig', array(
            'data' => $jsonContent
        ));
    }
}
