<?php

namespace Cremor\CremorBundle\Controller;

use Cremor\CremorBundle\Entity\AppelOffre;
use Cremor\CremorBundle\Entity\Devis;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class DevisController extends Controller
{
    public function addAction(Request $request)
    {
        $devis = new Devis();
        $formBuilder = $this->get('form.factory')->createBuilder(FormType::class, $devis);
        $formBuilder
            ->add('numDevis',TextType::class)
            ->add('date',TextType::class)
            ->add('bordereauxPrix',EntityType::class, array(
                'class'         => 'CremorBundle:BordereauxPrix',
                'choice_label'  => 'id',
            ))
            ->add('save',SubmitType::class)
        ;
        $form = $formBuilder->getForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($devis);
                $em->flush();
            }
        }
        return $this->render('CremorBundle:Devis:add.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $devis = $em->getRepository('CremorBundle:Devis')->find($id);
        $formBuilder = $this->get('form.factory')->createBuilder(FormType::class, $devis);
        $formBuilder
            ->add('numDevis',TextType::class)
            ->add('date',TextType::class)
            ->add('bordereauxPrix',EntityType::class, array(
                'class'         => 'CremorBundle:BordereauxPrix',
                'choice_label'  => 'id',
            ))
            ->add('save',SubmitType::class)
        ;
        $form = $formBuilder->getForm();

        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $em->flush();
        }

        return $this->render('CremorBundle:Devis:edit.html.twig', array(
            'devis' => $devis,
            'form'   => $form->createView(),
        ));
    }

    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $devis = $em->getRepository('CremorBundle:Devis')->find($id);
        $em->remove($devis);
        $em->flush();

        return $this->render('CremorBundle:Index:index.html.twig', array(
            'devis' => $devis,
        ));
    }

    public function listingAction()
    {
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);
        $repository = $this
            ->getDoctrine()
            ->getRepository('CremorBundle:Devis');

        $AOs = $repository->findAll();
        $jsonContent = $serializer->serialize($AOs, 'json');
        return $this->render('CremorBundle:Devis:devis.html.twig', array(
            'data' => $jsonContent
        ));
    }
}
